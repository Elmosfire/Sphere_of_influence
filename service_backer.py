
import structs
from random import choice
from rust_wrapper import InfluenceCache
import numpy as np
from types import SimpleNamespace
from random import sample
from tqdm import tqdm
from functools import reduce

def moved(array,dx,dy):
    return np.roll(np.roll(array,dx,axis=0),dy,axis=1)    

def named_moves(array):
    x = SimpleNamespace()
    x.up = moved(array,0,1)
    x.down = moved(array,0,-1)
    x.left = moved(array,1,0)
    x.right = moved(array,-1,0)
    return x

def moves(array):
    x = named_moves(array)
    return np.stack((x.up,x.down,x.left,x.right),axis=0)

def rollback_move(array):
    up,down,left,right = array

    return moved(down,0,1) + moved(up,0,-1) + moved(left,-1,0) + moved(right,1,0)
    
def get_borders(array):
    a = moves(array)
    x = ~np.all(a == a[0,:], axis = 0)
    x[0,:] = False
    x[-1,:] = False
    x[1,:] = False
    x[-2,:] = False
    return x
    
    
def load_world_data(seed):
    s = structs.BlobInteraction(f'worldgen/{seed}').download_json('travel')
    return structs.unpack_np(s['travel'],np.float,(512,512)),structs.unpack_np(s['allowed'],np.bool,(512,512))
    
    
class Regions:
    def __init__(self,state):
        self.state = state
    def owners(self):
        return self.state.argmax(axis=2)
    def scores(self):
        return {player:self.area(player)/512**2 for player in range(self.state.shape[2]) }
    def area(self,player):
        return (self.owners()==player).sum()
    def balance(self):
        return np.array([self.area(i) for i in range(self.state.shape[2])]).prod()
    def plt(self,player):
        area = self.owners()==player
        print('total area',self.area(player),player)
        infl = self.state[...,player]
        vclr = np.clip(1+infl/1024,0,1) * 0.3 * area
        vclr[get_borders(area) * area] = 0.8
        vclr[(infl>-10)] = 1
        return vclr * area
      
        
    
class Influence():
    def __init__(self,travel,allowed,nplayers):
        self.infl = [InfluenceCache(travel) for _ in range(int(nplayers))]
        self.allowed = allowed
    def save(self,blob):
        for i,v in enumerate(self.infl):
            prev = structs.package_np(v.prev)
            blob.upload_json(f"player_{i}",{'tokens':v.tokens,'prev':prev})
    def load(self,blob):
        for i,v in enumerate(self.infl):
            data = blob.download_json(f"player_{i}")
            v.tokens = data['tokens']
            v.prev = structs.unpack_np(data['prev'],np.uint64,(v.w,v.h))
            
    def valid(self,player):
        v = self.allowed & (self.state.owners() == player)
        assert v.shape,"Empty"
        return v
    
    def valid_border(self,player):
        return get_borders(self.valid(player)) & self.valid(player)    
    
    def place_token_player(self,player,x,y,force=False):
        if self.valid(player)[x,y] or force:
            print(x,y)
            self.infl[player].get_next(x,y)
            return True
        else:
            print('invalid token',x,y)
            return False
        
    def move_priority(self,player,x,y):
        return self.next_state(player,x,y).area(player)
    
    def move_priority_eq(self,player,x,y):
        return self.next_state(player,x,y).balance()
        
    def place_token_AI(self,player,tries):
        valid_border = self.valid_border(player)
        border = list(map(tuple,np.array(list(np.where(valid_border))).T))
        try:
            posmoves = sample(border,tries)
        except ValueError:
            posmoves = list(border)
        move_val = {}
        for xy in tqdm(posmoves):
            move_val[xy] = self.move_priority(player,*xy)
        x,y = max(posmoves,key=move_val.get)
        assert self.place_token_player(player,x,y),"AI performed invalid move"

        
    def place_token_balance(self,player,tries):
        valid_border = self.allowed
        border = list(map(tuple,np.array(list(np.where(valid_border))).T))
        try:
            posmoves = sample(border,tries)
        except ValueError:
            posmoves = list(border)
        move_val = {}
        for xy in tqdm(posmoves):
            move_val[xy] = self.move_priority_eq(player,*xy)
        x,y = max(posmoves,key=move_val.get)
        assert self.place_token_player(player,x,y,True),"AI performed invalid move"
        
    @property
    def state(self):
        return Regions(np.stack([x.cached() for x in self.infl],axis=2))
    
    def next_state(self,player,x,y):
        return Regions(np.stack([c.cached() if i != player else c.get_next(x,y,False) for i,c in enumerate(self.infl)],axis=2))

class Session:
    def __init__(self,session_id):
        self.session_id = session_id  
        self.interface = structs.SessionStore(self.session_id)
        self.blob_main = self.interface.maindata
        self.blob_stage = self.interface.stagedata
        self.settings = self.blob_main.download_json("settings")
        self.world_seed = self.settings["world"]
        self.world = structs.BlobInteraction(f'worldgen/{self.world_seed}')
        self.human_players = self.settings["human_players"]
        self.total_players = self.settings["human_players"] + self.settings["ai_players"]
        self.difficulty = self.settings["difficulty"]
        self.state = self.blob_stage.download_json("state")
        self.current_turn = self.state["turn"]
        self.player_active = self.state["player_active"]
        t,a = load_world_data(self.world_seed)
        self.infl = Influence(t,a,self.total_players)
        self.infl.load(self.blob_stage)
        
 
                
    def do_move(self,x,y):
        if self.infl.place_token_player(self.player_active,x,y):
            self.finish()
        
    def do_move_AI(self):
        self.infl.place_token_AI(self.player_active, self.difficulty)
        self.finish()
        
    def do_move_balance(self):
        self.infl.place_token_balance(self.player_active,20 if self.player_active else 1)
        self.finish()
        
    def plot(self):
        img = structs.ImageWrapper()
        img.add_from_blob(self.world,"worldmap")
        colors = [(0,0,1),(0,0.5,0),(1,0,0.5)]
        for i in range(self.total_players):
            print('plot',i)
            vlcr = self.infl.state.plt(i)
            img.add_from_np_color(colors[i],vlcr)
        img.save_to_blob(self.blob_stage,"worldmap")
        
    def finish(self):
        self.plot()
        self.infl.save(self.blob_stage)
        self.player_active += 1
        if self.player_active >= self.total_players:
            self.current_turn += 1
            self.player_active = 0
        self.blob_stage.upload_json("state",dict(
            player_active=self.player_active,
            turn=self.current_turn,
        ))
        with self.blob_stage.writer("final") as file:
            file.write('')
        
        
    
def build_session(session_id,human_players,AI_players,difficulty):
    data_layout = structs.bucket.list_blobs (prefix='worldgen')
    world_seed = choice([int(x.name.split('/')[1]) for x in data_layout if x.name.endswith('travel.json')])
    interface = structs.SessionStore(session_id)
    interface.maindata.upload_json("settings",
        dict(
            session_id=session_id,
            world=world_seed,
            human_players=human_players,
            ai_players=AI_players,
            difficulty=difficulty)
    )
    interface.stagedata.upload_json("state",dict(
        player_active=0,
        turn=0,
    ))
    travel,allowed = load_world_data(world_seed)
    with interface.stagedata.writer("final") as file:
        file.write('')
    
    Influence(travel,allowed,human_players+AI_players).save(interface.stagedata)
    SessionWrapper(session_id).init_field()
    
class SessionWrapper():
    def __init__(self,session_id):
        self.session_id = session_id
        
        
    @property
    def session(self):
        s= Session(self.session_id)
        self.player_active = s.player_active
        self.current_turn = s.current_turn
        self.human_players = s.human_players
        return s
    
    def perform_turn(self,player,x,y):
        self.session
        if self.current_turn >= 10:
            print('game is concluded')
            return
        if int(player) == self.player_active:
            if not self.session.do_move(int(y*512),int(x*512)):
                return
        else:
            print('not your turn',player,self.player_active)
        self.session
        while self.player_active >= self.human_players:
            print('ai move',self.player_active)
            self.session.do_move_AI()
            self.session
                
    def init_field(self):
        self.session
        while not self.current_turn:
            print(self.current_turn)
            self.session.do_move_balance()
            self.session
            
    def get_picture(self):
        img = structs.ImageWrapper()
        img.add_from_blob(self.session.blob_stage,"worldmap")
        return img.serve_file()
    
    
if __name__ == '__main__':
    build_session(250,1,1,10)