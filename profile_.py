from engine import Session
import cProfile


def main():
    s = Session.get('profile')
    s.process_move_rel(0.7,0.7)
    s.process_move_rel(0.4,0.4)
    s.process_move_rel(0.3,0.5)
    
    
    
    
    
if __name__ == "__main__":
    pr = cProfile.Profile()
    pr.enable()
    main()
    pr.disable()
    pr.dump_stats('test.profile') 