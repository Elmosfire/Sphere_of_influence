mkdir -p ./world
cd ./world

worldengine --hdf5 -s $1
mv seed_$1.world seed_$1.hdf
echo '' > seed_$1.seed
