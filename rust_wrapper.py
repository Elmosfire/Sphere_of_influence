import rust_influence
import numpy as np
import base64
import json

#x = np.zeros((3,3)) - float('inf')


#travel = np.random.rand(500,500)*10 + 1.0

#travel = np.ones((500,500))


def get_influence(tokens,travel,mult=1000):
    w,h = travel.shape
    prev = np.zeros((w,h))
    for x,y in tokens:
        prev = rust_influence.expand(x,y,((travel+1.0)*mult).astype(np.uint64),prev.astype(np.uint64),w,h)
    #prev = prev.astype(np.int64)
    return -(~prev).astype(np.float)/mult

class InfluenceCache():
    def __init__(self,travel,mult=1000):
        self.travel = ((travel+1.0)*mult).astype(np.uint64)
        self.mult = mult
        self.tokens = []
        self.w,self.h = travel.shape
        self.prev = np.zeros(travel.shape).astype(np.uint64)
    def get_next(self,x,y,cache=True):
        res = rust_influence.expand(x,y,self.travel,self.prev,self.w,self.h)
        if cache:
            self.prev = res
            self.tokens.append((int(x),int(y)))
        return -(~res).astype(np.float)/self.mult
    def cached(self):
        return -(~self.prev).astype(np.float)/self.mult

        
    
if __name__ == "__main__":
    travel = np.ones((7,7))
    travel[:,4] = 6
    print(get_influence([(1,1)], travel))
    print(get_influence([(1,2)], travel))