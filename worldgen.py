
from random import randrange
import structs
from subprocess import Popen
import h5py 
import numpy as np
from types import SimpleNamespace
from pathlib import Path

def moved(array,dx,dy):
    return np.roll(np.roll(array,dx,axis=0),dy,axis=1)    

def named_moves(array):
    x = SimpleNamespace()
    x.up = moved(array,0,1)
    x.down = moved(array,0,-1)
    x.left = moved(array,1,0)
    x.right = moved(array,-1,0)
    return x

def moves(array):
    x = named_moves(array)
    return np.stack((x.up,x.down,x.left,x.right),axis=0)

def rollback_move(array):
    up,down,left,right = array

    return moved(down,0,1) + moved(up,0,-1) + moved(left,-1,0) + moved(right,1,0)
    
def get_borders(array):
    a = moves(array)
    x = ~np.all(a == array[np.newaxis,...], axis = 0)
    x[0,:] = False
    x[-1,:] = False
    x[1,:] = False
    x[-2,:] = False
    return x


def gen_seed(seed=0):
    if not Path(f'world/seed_{seed}.hdf').exists():
        Popen(('bash','generate_world.sh',str(seed))).wait()
    f = h5py.File(f'world/seed_{seed}.hdf', 'r')
    
    #elevation = f["elevation"]["data"].value.astype(np.float)
    ocean = f["ocean"][()].astype(np.bool)
    rivers = f["river_map"][()] > 0
    
    
    #elevation_change = moves(elevation) - elevation[np.newaxis,...]
    
    #elevation_change_c = (elevation_change**2).sum(axis=0)
    
    #elevation in ocean is zero
    #elevation_change_c *= 1-ocean
    
    #elevation_change_c /= elevation_change_c.max()
    
    #water
    water = ocean | rivers
    
    #travel
    travel = np.ones(ocean.shape)#+ elevation_change_c*9 
    
    #travel over water is always 12
    travel[water] = 12
    
    allowed = ~water
    
    worlddata = dict(
        travel=structs.package_np(travel),
        allowed=structs.package_np(allowed),
        )
    
    blob = structs.BlobInteraction(f'worldgen/{seed}')
    
    blob.upload_json('travel',worlddata)
    
    mapdisp = structs.ImageWrapper()
    
    mapdisp.add_color((1,1,1),travel.shape)
    
    #mapdisp.add_from_np_color((0,0,0),elevation/elevation.max())
    mapdisp.add_from_np_color((0,0,0),(~water)*get_borders(water))
    mapdisp.add_from_np_color((0,0.5,1),water*0.2)
    
    mapdisp.save_to_blob(blob,'worldmap')
    
    
    
    
if __name__ == "__main__":
    for i in range(1000):
        gen_seed(i)
    
    
    
    

    
