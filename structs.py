


from PIL import Image
from io import BytesIO,StringIO

from google.cloud import storage
import base64
import numpy as np
import json

try:
    client = storage.Client()
except:
    print('loading credentials from keyfile')
    client = storage.Client.from_service_account_json('keyfile.json')
bucket = client.get_bucket('sphere-of-influence-map-storage')


def package_np(array):
    return base64.b64encode(array).decode('utf-8')


def unpack_np(string,type_,size):
    return np.frombuffer(base64.b64decode(string),dtype=type_).reshape(size)


class SessionStore:
    def __init__(self,session_id):
        self.root = f"sessionstore/{session_id}"
        data_layout = bucket.list_blobs (prefix=self.root)
        try:
            self.stage = max(int(x.name.split('/')[2]) for x in data_layout if x.name.endswith('final'))
        except ValueError:
            self.stage = -1
        print('stage',self.stage)
        self.maindata = BlobInteraction(f"{self.root}/main")
        self.stagedata = BlobInteraction(f"{self.root}/{self.stage}",f"{self.root}/{self.stage + 1}")
    @property
    def valid(self):
        return self.stage > -1
        
        
class BlobInteraction:
    def __init__(self,read,write=None):
        self.read = read
        self.write = write if write is not None else read
    def writer(self,fname):
        return StoreWriter(f"{self.write}/{fname}")
    def reader(self,fname):
        return StoreReader(f"{self.write}/{fname}")
    def upload_json(self,fname,data):
        bucket.blob(f"{self.write}/{fname}.json").upload_from_string(json.dumps(data))
            
    def download_json(self,fname):
        return json.loads(bucket.blob(f"{self.read}/{fname}.json").download_as_string())

class StoreReader:
    def __init__(self,root,binary=False):
        print(root)
        self.blob = bucket.blob(root)
        self.io = [StringIO,BytesIO][binary]()
        
    def __enter__(self):
        print(self.io)
        self.blob.download_to_file(self.io)
        self.io.seek(0)
        return self.io
    
    def __exit__(self,*args):
        self.io.close()
        
class StoreWriter:
    def __init__(self,root,binary=False):
        self.blob = bucket.blob(root)
        self.io = [StringIO,BytesIO][binary]()
        
    def __enter__(self):
        return self.io
    
    def __exit__(self,*args):
        self.io.flush()
        self.io.seek(0)
        self.blob.upload_from_file(self.io)
        self.io.close()

class ImageWrapper:
    def __init__(self):
        self.image = None
    def add_image(self,img):
        if self.image is None:
            self.image = img.copy()
        else:
            self.image.paste(img, (0, 0), img)  
            
    def add_from_file(self,filename):
        self.add_image(Image.open(filename))
        
    def add_from_np(self,array):
        self.add_image(Image.fromarray(np.uint8(array*255)))
        
    def add_from_np_color(self,color,array):
        r,g,b = color
        s1 = np.ones(array.shape)
        array = np.stack([r*s1,g*s1,b*s1,array],axis=2)
        self.add_from_np(array)
        
    def add_color(self,color,size):
        r,g,b = color
        s1 = np.ones(size)
        array = np.stack([r*s1,g*s1,b*s1,s1],axis=2)
        self.add_from_np(array)
        
    def add_from_np_stack(self,*arrays):
        for array in arrays:
            self.add_from_np(array)
            
    def upscaled(self):
        return self.image.resize((2048,2048), Image.NEAREST)    
    
    def add_from_blob(self,bi,fname):
        with StoreReader(f"{bi.read}/{fname}.png",True) as file:
            self.add_image(Image.open(file))
            
    def save_to_blob(self,bi,fname):
        with StoreWriter(f"{bi.write}/{fname}.png",True) as file:
            print(file)
            self.image.save(file,'png')
            
    def serve_file(self):
        file = BytesIO()
        self.upscaled().save(file,'png')
        file.seek(0)
        return file