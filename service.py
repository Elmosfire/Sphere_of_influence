# serve.py

from flask import Flask
from flask import render_template, request, session, redirect, url_for, Response, make_response, send_file

from service_backer import SessionWrapper, build_session
from random import randrange
import secrets
import json
import os


# creates a Flask application, named app
app = Flask(__name__)
app.config["SECRET_KEY"] = secrets.token_urlsafe(16)


@app.route("/")
def get():
    session_id = request.args.get('session_id')
    if session_id is None:
        return make_response(render_template('loading.html'))
    response = make_response(render_template('picture.html'))
    response.headers['Pragma-Directive'] = 'no-cache'
    response.headers['Cache-Directive'] = 'no-cache'
    response.headers['Cache-Control'] = 'no-cache, no-store, must-revalidate'
    response.headers['Pragma'] = 'no-cache'
    response.headers['Expires'] = '0'
    return response

@app.route("/create/")
def create():
    session_id = randrange(16**16)
    players = request.args.get('players')
    ais = request.args.get('AIs')
    diff = request.args.get('diff')
    build_session(session_id,int(players),int(ais),int(diff))
    return redirect(f'/?session_id={session_id}&player=0')

@app.route("/",methods=['POST'])
def post():
    
    print(request.json)
    data = request.json
    s = SessionWrapper(data['session_id'])
    if 'x' in data and 'y' in data:
        s.perform_turn(data['player'],data['x'],data['y'])
    else:
        s.perform_turn(-100,0,0)

    return Response(json.dumps({'success':True}), 200, {'ContentType':'application/json'} )

@app.route("/picture/<session_id>/",methods=['GET'])
def get_pic(session_id):
    picture = SessionWrapper(session_id).get_picture()
    return send_file(picture,mimetype='png')
    #stage = SessionWrapper(session_id).session.interface.stage
    #return redirect(f'https://storage.googleapis.com/sphere-of-influence-map-storage/sessionstore/{session_id}/{stage}/worldmap.png')

@app.route("/scores/<session_id>/",methods=['GET'])
def get_scores(session_id):
    session = SessionWrapper(session_id).session
    a = session.infl.state.scores()
    b = session.player_active if session.current_turn < 10 else -1
    b = {'red':f'{a[0]:.1%}','blue':f'{a[1]:.1%}', 'active':b}
    return Response(json.dumps(b), 200, {'ContentType':'application/json'})


app.run(host='0.0.0.0', port=os.environ.get("PORT",8080), debug=True)