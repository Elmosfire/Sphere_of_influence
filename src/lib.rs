use ndarray::{ArrayD, ArrayViewD};
use numpy::{IntoPyArray, PyArrayDyn, PyReadonlyArrayDyn};
use pyo3::prelude::{pymodule, PyModule, PyResult, Python};
use priority_queue::PriorityQueue;


struct ExpandQ {
    q:PriorityQueue<(usize,usize),u64>
}

impl ExpandQ {
    fn new() -> ExpandQ {
        ExpandQ{q:PriorityQueue::new()}
    }
    fn store(&mut self,x:usize,y:usize,val:u64) {
        let old = self.q.get_priority(&(x,y));
        match old {
            Some(a) => {
                if a < &val {
                    self.q.change_priority(&(x,y),val);
                }
            }
            None => {self.q.push((x,y),val);}
        }
    }
    fn get(&mut self) -> (usize,usize,u64) {
        let ((x,y),val) = self.q.pop().unwrap();
        (x,y,val)
    }
}

#[pymodule]
fn rust_influence(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    // immutable example
    fn expand(x: usize, y:usize, travel: ArrayViewD<'_, u64>, prev: ArrayViewD<'_, u64>, mxw: usize, mxh:usize) -> ArrayD<u64> {
        let mut q = ExpandQ::new();
        q.store(x,y,u64::MAX);

        let mut infl = prev.to_owned();

        while q.q.len() > 0 {
            
            let (x,y,val) = q.get();
            //println!("val {}",val);
            let l = x-1;
            let r = x+1;
            let t = (y-1)%mxh;
            let b = (y+1)%mxh;
            let nval = val - travel[[x,y]];
            let nvaldiag = val - travel[[x,y]] - travel[[x,y]]/2;
            if val > infl[[x,y]] {
                infl[[x,y]] = val;
                if x > 0 {
                    q.store(l,y,nval);
                    q.store(l,t,nvaldiag);
                    q.store(l,b,nvaldiag);
                }
                if x < mxw-1 {
                    q.store(r,y,nval);
                    q.store(r,t,nvaldiag);
                    q.store(r,b,nvaldiag);
                }
                q.store(x,t,nval);
                q.store(x,b,nval);
                
            }

        };
        infl

    }

    // wrapper of `axpy`
    #[pyfn(m, "expand")]
    fn expand_py<'py>(
        py: Python<'py>,
        x: usize,
        y: usize,
        travel: PyReadonlyArrayDyn<u64>,
        prev: PyReadonlyArrayDyn<u64>,
        mxw: usize,
        mxh: usize
    ) -> &'py PyArrayDyn<u64> {
        let travel = travel.as_array();
        let prev = prev.as_array();
        expand(x,y,travel,prev,mxw,mxh).into_pyarray(py)
    }


    Ok(())
}