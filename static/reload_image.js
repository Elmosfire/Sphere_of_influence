$(document).ready(function() {

    

    base_image = img.src
    const urlParams = new URLSearchParams(window.location.search);
    const session_id = urlParams.get('session_id');
    const player = urlParams.get('player');

    reload_picture = function() {
        
        img.src = '/picture/' + session_id + '/' + '?t=' + new Date().getTime();
        $.get(
            '/scores/' + session_id + '/',
            function(data) {
                console.log(data);
                ret = JSON.parse(data);
                $('.redscore').html(ret.red)
                $('.bluescore').html(ret.blue)
                if (ret.active == player) {
                    $('.active').html("your turn")
                }
                else if (ret.active == -1)
                {
                    $('.active').html("Game is concluded")
                }
                else
                {
                    $('.active').html("players "+ret.active+'\'s turn')
                }
            }
        );
        
        
    };

    send_action = function(payload, onsuccess) {
        reload_picture();
        $("svg").css("visibility", "visible")
        var url = "/";
        var xhr = new XMLHttpRequest();
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4 && xhr.status === 200) {
                //img.src = 'static/pic_'+session_id+'.png?t=' + new Date().getTime();
                $("svg").css("visibility", "hidden"); 
                ret = JSON.parse(xhr.responseText);
                
                console.log("success",onsuccess,img,img.src);
                reload_picture();
                onsuccess();
            }
            else
            {
                $("svg").css("visibility", "hidden"); 
                console.log(xhr.status);
            }
        }
        var data = JSON.stringify(payload);
        xhr.send(data);
    }

    send_poke = function() {send_action({'session_id':session_id}, function(){})};
    send_player = function(x,y) {send_action({'x':x,'y':y,'player':player,'session_id':session_id}, send_poke)};
    
    
    setInterval(reload_picture, 10000);

    $("svg").css("visibility", "hidden"); 

   


    $("img").on("click", function(event) {
        bounds=this.getBoundingClientRect();
        var left=bounds.left;
        var top=bounds.top;
        var x = event.pageX - left;
        var y = event.pageY - top;
        var cw=this.clientWidth
        var ch=this.clientHeight
        var iw=this.naturalWidth
        var ih=this.naturalHeight
        var px=x/cw*iw
        var py=y/ch*ih
        
        send_player(px/iw,py/ih);
        
        

        
        
    });
});
